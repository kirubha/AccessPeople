/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.controller;

import com.api.db.service.ManageClients;
import com.api.db.service.ManageResults;
import com.api.db.service.ManageSummary;
import com.api.db.service.ManageUsers;
import com.api.entities.ClientsBean;
import com.api.entities.ResultBean;
import com.api.entities.SummaryBean;
import com.api.entities.UsersBean;
import com.api.utils.Dolog;
import com.api.utils.StaticValues;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author suresh
 */
public class ResultController {

    private static final Dolog log = new Dolog(ResultController.class);
    ManageResults manageResults = new ManageResults();
    ManageSummary manageSummary = new ManageSummary();
    ManageUsers manageUsers = new ManageUsers();
    ResultBean resultBean = new ResultBean();
    Gson gson = new Gson();
    JSONObject jsonobject = new JSONObject();

    public String getClientBasedResult(String client_id) {
        ArrayList<ResultBean> getResultBeans = new ArrayList<>();
        ArrayList<ResultBean> resultBeans = new ArrayList<>();
        try {
            getResultBeans = (ArrayList<ResultBean>) manageResults.getUsersByClient_id(client_id);
            if (getResultBeans != null) {
                for (ResultBean rb : getResultBeans) {
                    resultBean = new ResultBean();
                    resultBean.setUser_id(rb.getUser_id());
                    resultBean.setCorrect_answers(rb.getCorrect_answers());
                    resultBean.setWrong_answers(rb.getWrong_answers());
                    resultBean.setExam_timetaken(rb.getExam_timetaken());
                    resultBean.setUser_marks(rb.getUser_marks());
                    resultBeans.add(resultBean);
                }
                return gson.toJson(resultBeans);
            } else {
                jsonobject.put("resCode", StaticValues.RESULTS_NOT_FOUND);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("011"));
                return jsonobject.toString();
            }
        } catch (Exception e) {
            log.info("Exp : " + e.toString());
            try {
                jsonobject.put("resCode", StaticValues.SERVER_ERROR);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("003"));
                return jsonobject.toString();
            } catch (JSONException ex) {
                Logger.getLogger(ResultController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public String getIPBasedResult(String ip) {
        ArrayList<ClientsBean> getResultBeans = new ArrayList<>();
        ArrayList<ResultBean> resultBeans = new ArrayList<>();
        ManageClients manageClients = new ManageClients();
        try {

            ClientsBean clientsBean = manageClients.getUserByClientIP(ip);
            if (clientsBean != null) {
                return getClientBasedResult(clientsBean.getClient_id());
            } else {
                jsonobject.put("resCode", StaticValues.RESULTS_NOT_FOUND);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("011"));
                return jsonobject.toString();
            }
//            if (getResultBeans != null) {
//                for (ResultBean rb : getResultBeans) {
//                    resultBean = new ResultBean();
//                    resultBean.setUser_id(rb.getUser_id());
//                    resultBean.setCorrect_answers(rb.getCorrect_answers());
//                    resultBean.setWrong_answers(rb.getWrong_answers());
//                    resultBean.setExam_timetaken(rb.getExam_timetaken());
//                    resultBean.setUser_marks(rb.getUser_marks());
//                    resultBeans.add(resultBean);
//                }
//                return gson.toJson(resultBeans);
//            } else {
//                jsonobject.put("resCode", StaticValues.RESULTS_NOT_FOUND);
//                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("011"));
//                return jsonobject.toString();
//            }
        } catch (Exception e) {
            log.info("Exp : " + e.toString());
            try {
                jsonobject.put("resCode", StaticValues.SERVER_ERROR);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("003"));
                return jsonobject.toString();
            } catch (JSONException ex) {
                Logger.getLogger(ResultController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public String getUserBasedResult(String auth_key) {
        try {
            log.info("Entered User Based");
            UsersBean usersBean = manageUsers.getUserByAuth_key(auth_key);
            if (usersBean != null) {
                ResultBean getResultBean = manageResults.getUserByuser_id(usersBean.getUser_id(), usersBean.getClient_id());
                if (getResultBean != null) {
                    log.info("Enter user in not null");
                    getResultBean.setCreated_date(null);
                    getResultBean.setUpdated_date(null);
                    return gson.toJson(getResultBean);
                } else {
                    jsonobject.put("resCode", StaticValues.RESULTS_NOT_FOUND);
                    jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("011"));
                    return jsonobject.toString();
                }
            } else {
                try {
                    log.info("Auth Key Failure ");
                    jsonobject.put("resCode", StaticValues.AUTH_KEY_FAILURE);
                    jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("004"));
                } catch (JSONException ex) {
                    Logger.getLogger(QuestionController.class.getName()).log(Level.SEVERE, null, ex);
                }
                return jsonobject.toString();
            }
        } catch (Exception e) {
            log.info("User Based Exp : " + e.toString());
            try {
                jsonobject.put("resCode", StaticValues.SERVER_ERROR);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("003"));
                return jsonobject.toString();
            } catch (JSONException ex) {
                Logger.getLogger(ResultController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public String getUserResult(String auth_key) {
        ArrayList<ResultBean> resultBeans = new ArrayList<>();
        try {
            log.info("Entered User Based");
            UsersBean usersBean = manageUsers.getUserByAuth_key(auth_key);
            if (usersBean != null) {
                SummaryBean summaryBean = manageSummary.getUserByuser_id(usersBean.getUser_id(), usersBean.getClient_id());
                if (summaryBean != null) {
                    log.info("Enter user in not null");
                    int crtAns = manageSummary.getCountAnsStatus("C", usersBean.getUser_id());
                    int wrngAns = manageSummary.getCountAnsStatus("W", usersBean.getUser_id());
                    int marks = manageSummary.getSumMarks(usersBean.getUser_id());
                    resultBean.setCorrect_answers(crtAns);
                    resultBean.setWrong_answers(wrngAns);
                    resultBean.setExam_timetaken(summaryBean.getExam_timetaken());
                    resultBean.setUser_marks(marks);
                    return gson.toJson(resultBean);
                } else {
                    jsonobject.put("resCode", StaticValues.RESULTS_NOT_FOUND);
                    jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("011"));
                    return jsonobject.toString();
                }
            } else {
                try {
                    log.info("Auth Key Failure ");
                    jsonobject.put("resCode", StaticValues.AUTH_KEY_FAILURE);
                    jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("004"));
                } catch (JSONException ex) {
                    Logger.getLogger(QuestionController.class.getName()).log(Level.SEVERE, null, ex);
                }
                return jsonobject.toString();
            }
        } catch (Exception e) {
            log.info("User Based Exp : " + e.toString());
            try {
                jsonobject.put("resCode", StaticValues.SERVER_ERROR);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("003"));
                return jsonobject.toString();
            } catch (JSONException ex) {
                Logger.getLogger(ResultController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public String getClientResult(String client_id) {
        ArrayList<SummaryBean> summaryBeans = new ArrayList<>();
        ArrayList<ResultBean> resultBeans = new ArrayList<>();
        try {
            summaryBeans = (ArrayList<SummaryBean>) manageSummary.getUsersByClient_id(client_id);
            if (summaryBeans != null) {
                for (SummaryBean sb : summaryBeans) {
                    int crtAns = manageSummary.getCountAnsStatus("C", sb.getUser_id());
                    int wrngAns = manageSummary.getCountAnsStatus("W", sb.getUser_id());
                    int marks = (int) manageSummary.getSumMarks(sb.getUser_id());
                    resultBean = new ResultBean();
                    resultBean.setUser_id(sb.getUser_id());
                    resultBean.setCorrect_answers(crtAns);
                    resultBean.setWrong_answers(wrngAns);
                    resultBean.setExam_timetaken(sb.getExam_timetaken());
                    resultBean.setUser_marks(marks);
                    resultBeans.add(resultBean);
                }
                return gson.toJson(resultBeans);
            } else {
                jsonobject.put("resCode", StaticValues.RESULTS_NOT_FOUND);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("011"));
                return jsonobject.toString();
            }
        } catch (Exception e) {
            log.info("Exp : " + e.toString());
            try {
                jsonobject.put("resCode", StaticValues.SERVER_ERROR);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("003"));
                return jsonobject.toString();
            } catch (JSONException ex) {
                Logger.getLogger(ResultController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

}
