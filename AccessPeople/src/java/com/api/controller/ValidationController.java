/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.controller;

import com.api.db.service.ManageUsers;
import com.api.entities.UsersBean;
import com.api.utils.Dolog;
import com.api.utils.Utils;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 *
 * @author suresh
 */
public class ValidationController {

    private static final Dolog log = new Dolog(ValidationController.class);
    ManageUsers manageUsers = new ManageUsers();
    Utils utils = new Utils();
    boolean result = false;

    public boolean authValidation(UsersBean usersBean, String auth_key) {
        try {
            log.info("Auth_key : " + auth_key);
            log.info("User Id : " + usersBean.getUser_id());
            UsersBean ub = manageUsers.getUserByClntwithUsr_id(usersBean.getClient_id(), usersBean.getUser_id());
            if (ub != null) {
                if (ub.getAuth_key().equals(auth_key)) {
                    log.info("Auth key Equals");
                    return true;
                }
            }
        } catch (Exception e) {
            log.info("Exp : " + e.toString());
            return false;
        }
        return false;
    }

    public boolean timeValidation(UsersBean usersBean) {
        try {
            log.info("Time Created : " + usersBean.getCreated_date());
            log.info("Current Time : " + utils.getTimeStamp());
            long diffMinits = utils.diffMinits(utils.getTimeStamp(), utils.convertDatetoString(usersBean.getCreated_date()));
            log.info("Difference Minits : " + diffMinits);
            log.info("Validate Secs : " + usersBean.getValidate_secs());
            if (diffMinits != 0) {
                if (usersBean.getValidate_secs() > diffMinits || usersBean.getValidate_secs() == 0) {
                    log.info("Validate Sec True ");
                    return true;
                }
            }
        } catch (Exception e) {
            log.info("EXP : " + e.toString());
            return false;
        }
        return false;
    }

    public static void main(String[] args) {
        int TEN_MINUTES = 10 * 60 * 1000;
        System.out.println("" + TEN_MINUTES);
        System.out.println("" + System.currentTimeMillis());
        long tenAgo = System.currentTimeMillis() - TEN_MINUTES;
        if (5 < tenAgo) {
            System.out.println("searchTimestamp is older than 10 minutes");
        }
    }

}
