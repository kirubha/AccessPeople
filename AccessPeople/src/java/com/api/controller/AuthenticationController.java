/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.controller;

import com.api.db.service.ManageClients;
import com.api.db.service.ManageUsers;
import com.api.entities.ClientsBean;
import com.api.entities.UsersBean;
import com.api.utils.Dolog;
import com.api.utils.StaticValues;
import com.api.utils.Utils;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author suresh
 */
public class AuthenticationController {

    private static final Dolog log = new Dolog(AuthenticationController.class);
    ManageClients manageClients = new ManageClients();
    ManageUsers manageUsers = new ManageUsers();
    UsersBean usersBean = new UsersBean();
    Utils utils = new Utils();
    JSONObject jsonobject = new JSONObject();
    int id = 0;

    public String validateClients(ClientsBean clientsBean) {
        log.info("Client id : " + clientsBean.getClient_id());
        log.info("Client name : " + clientsBean.getUser_name());
        log.info("Client Pwd : " + clientsBean.getClient_password());
        try {
            ClientsBean bean = manageClients.validateClient(clientsBean.getClient_id(), clientsBean.getClient_password());
            if (bean != null) {
                log.info("Entered to check");
                String auth_key = utils.genAuth_key();
                usersBean.setAuth_key(auth_key);
                usersBean.setClient_id(clientsBean.getClient_id());
                if (clientsBean.getDuration() != 0) {
                    usersBean.setValidate_secs(clientsBean.getDuration());
                } else {
                    usersBean.setValidate_secs(0);
                }
                usersBean.setUser_name(clientsBean.getUser_name());
                usersBean.setUser_exam_status('N');
                usersBean.setCreated_date(utils.genTimeStamp());
                log.info("Created Date : " + usersBean.getCreated_date());
                id = manageUsers.addUser(usersBean);
                log.info("User created : " + id);
                if (id > 0) {
                    UsersBean ub = manageUsers.getUserByUser_id(id);
                    jsonobject.put("auth_key", ub.getAuth_key());
                    jsonobject.put("user_id", ub.getUser_id());
                    jsonobject.put("resCode", StaticValues.SUCCESS);
                    log.info("Success Res : " + jsonobject.toString());
                    return jsonobject.toString();
                }

            } else {
                jsonobject.put("resCode", StaticValues.USER_NOT_FOUND);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("008"));
                log.info("Failure Res : " + jsonobject.toString());
                return jsonobject.toString();
            }
        } catch (Exception e) {
            try {
                jsonobject.put("resCode", StaticValues.SERVER_ERROR);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("003"));
            } catch (JSONException ex) {
                Logger.getLogger(AuthenticationController.class.getName()).log(Level.SEVERE, null, ex);
            }
            log.info("Authendication Exp : " + jsonobject.toString());
            return jsonobject.toString();
        }
        return null;
    }

}
