/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.controller;

import com.api.db.service.ManageAnswers;
import com.api.db.service.ManageQuestions;
import com.api.db.service.ManageResults;
import com.api.db.service.ManageSummary;
import com.api.db.service.ManageUsers;
import com.api.entities.AnswerBean;
import com.api.entities.QuestionBean;
import com.api.entities.ResponseBean;
import com.api.entities.ResultBean;
import com.api.entities.SummaryBean;
import com.api.entities.UsersBean;
import com.api.utils.Dolog;
import com.api.utils.StaticValues;
import com.api.utils.Utils;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author suresh
 */
public class QuestionController {

    private static final Dolog log = new Dolog(QuestionController.class);
    ManageUsers manageUsers = new ManageUsers();
    ManageQuestions manageQuestions = new ManageQuestions();
    ManageSummary manageSummary = new ManageSummary();
    ManageAnswers manageAnswers = new ManageAnswers();
    ManageResults manageResults = new ManageResults();
    SummaryBean summaryBean = new SummaryBean();
    ResponseBean responseBean = new ResponseBean();
    ValidationController validationController = new ValidationController();
    Utils utils = new Utils();
    JSONObject jsonobject = new JSONObject();
    Gson gson = new Gson();
    int id = 0;

    public String getQuestions(QuestionBean questionBean, String auth_key) {
        ArrayList<QuestionBean> listQuestions = new ArrayList<>();
        ArrayList<SummaryBean> summaryBeans = new ArrayList<>();
        boolean batchres = false;
        boolean result = false;
        try {
            log.info("Auth_key : " + auth_key);
            UsersBean usersBean = manageUsers.getUserByAuth_key(auth_key);
            if (usersBean != null) {
                result = validationController.timeValidation(usersBean);
                log.info("Validation Response : " + result);
            } else {
                try {
                    log.info("Auth Key Failure ");
                    jsonobject.put("resCode", StaticValues.AUTH_KEY_FAILURE);
                    jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("004"));
                } catch (JSONException ex) {
                    Logger.getLogger(QuestionController.class.getName()).log(Level.SEVERE, null, ex);
                }
                return jsonobject.toString();
            }
            if (result) {
                log.info("User Id : " + usersBean.getUser_id());
                log.info("Client Id : " + usersBean.getClient_id());
                SummaryBean sumBean = manageSummary.getUserByuser_id(usersBean.getUser_id(), usersBean.getClient_id());
                if (sumBean == null) {
                    if (questionBean.getTotal_no_qus() != 0) {
                        log.info("Module Code : " + questionBean.getModuleCode());
                        log.info("Total Qus : " + questionBean.getTotal_no_qus());
                        log.info("Req Qus : " + questionBean.getRequest_qus());
                        listQuestions = (ArrayList<QuestionBean>) manageQuestions.getQuesByModCodeWithTotalQus(questionBean.getModuleCode(), questionBean.getTotal_no_qus());
                        log.info("List Qus Size : " + listQuestions.size());
                        if (listQuestions.size() > 0) {
                            for (QuestionBean qb : listQuestions) {
                                summaryBean = setToSummary(qb, usersBean);
                                summaryBeans.add(summaryBean);
                            }
                            batchres = manageSummary.batchInsert(summaryBeans);
                        } else {
                            jsonobject.put("resCode", StaticValues.MODULE_CODE_NOT_FOUND);
                            jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("012"));
                            return jsonobject.toString();
                        }
                    } else {
                        jsonobject.put("resCode", StaticValues.IMPORTANT_FIELD_MISSING);
                        jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("010"));
                        return jsonobject.toString();
                    }
                } else {//User Play a quiz after some break if time is possible
                    return getReqQus(questionBean, usersBean);
                }
                log.info("Batch res : " + batchres);
                if (batchres) {
                    ResultBean resultBean = new ResultBean();
                    resultBean.setClient_id(summaryBean.getClient_id());
                    resultBean.setUser_id(summaryBean.getUser_id());
                    resultBean.setModuleCode(summaryBean.getModuleCode());
                    resultBean.setCreated_date(utils.genTimeStamp());
                    int res = manageResults.addResult(resultBean);
                    log.info("ADD Result Tb from : " + res);
                    return getReqQus(questionBean, usersBean);
                } else {
                    try {
                        log.info("Qus not found");
                        jsonobject.put("resCode", StaticValues.QUS_NOT_FOUND);
                        jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("006"));
                    } catch (JSONException ex) {
                        Logger.getLogger(QuestionController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return jsonobject.toString();
                }

            } else {
                try {
                    log.info("Time Expired");
                    jsonobject.put("resCode", StaticValues.TIME_EXPIRED);
                    jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("009"));
                } catch (JSONException ex) {
                    Logger.getLogger(QuestionController.class.getName()).log(Level.SEVERE, null, ex);
                }
                return jsonobject.toString();
            }
        } catch (Exception e) {
            try {
                log.info("Qus Exp : " + e.toString());
                jsonobject.put("resCode", StaticValues.SERVER_ERROR);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("003"));
            } catch (JSONException ex) {
                Logger.getLogger(QuestionController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return jsonobject.toString();
        }
    }

    public SummaryBean setToSummary(QuestionBean qb, UsersBean usersBean) {
        log.info("Enterd to set summary");
        try {
            summaryBean = new SummaryBean();
            summaryBean.setQtnIntCode(qb.getQtnIntCode());
            summaryBean.setAnsIntCode(qb.getAnsIntCode());
            summaryBean.setModuleCode(qb.getModuleCode());
            summaryBean.setClient_id(usersBean.getClient_id());
            summaryBean.setUser_id(usersBean.getUser_id());
            summaryBean.setCreated_date(utils.genTimeStamp());
            summaryBean.setUser_marks(0);
            summaryBean.setQus_status("N");
        } catch (Exception e) {
            log.info("setToSummary Exp : " + e.toString());
        }
        return summaryBean;
    }

    public String getReqQus(QuestionBean questionBean, UsersBean usersBean) {
        ArrayList<QuestionBean> listQuestions = new ArrayList<>();
        ArrayList<SummaryBean> reqQuestions = new ArrayList<>();
        ArrayList<AnswerBean> listAnswers = new ArrayList<>();
        ArrayList<AnswerBean> answerBeans = new ArrayList<>();
        ArrayList<ResponseBean> responseBeans = new ArrayList<>();
        try {
            log.info("Entered to Req Qus Method ");
            reqQuestions = (ArrayList<SummaryBean>) manageSummary.getRandQusID(questionBean.getRequest_qus(), usersBean.getUser_id(), usersBean.getClient_id());
            if (reqQuestions.size() > 0) {
                for (SummaryBean sb : reqQuestions) {
                    responseBean = new ResponseBean();
                    sb.setQus_status("S");
                    boolean reqQusRes = sb.dataSave();
                    if (reqQusRes) {
                        listQuestions = (ArrayList<QuestionBean>) manageQuestions.getQuestionsByQusIntCode(sb.getQtnIntCode());
                        if (listQuestions != null) {
                            if (listQuestions.size() > 0) {
                                QuestionBean qusBean = new QuestionBean();
                                qusBean.setQtnIntCode(listQuestions.get(0).getQtnIntCode());
                                if (listQuestions.get(0).getQtnType().equalsIgnoreCase("T")) {
                                    qusBean.setTextQtn(listQuestions.get(0).getTextQtn());
                                } else {
                                    qusBean.setImg_url(listQuestions.get(0).getImg_url());
                                }
                                responseBean.setQuestionBean(qusBean);
                                listAnswers = (ArrayList<AnswerBean>) manageAnswers.getAnsByQusIntCode(sb.getQtnIntCode());
                                log.info("entered listAnswers size : " + listAnswers.size());
                                for (AnswerBean bean : listAnswers) {
                                    answerBeans = new ArrayList<>();
                                    if (bean.getAnsType().equalsIgnoreCase("T")) {
                                        bean.setTextAns(bean.getTextAns());
                                    } else {
                                        bean.setImgAns(bean.getImgAns());
                                    }
                                    bean.setUpdated_date(null);
                                    answerBeans.add(bean);
                                }
                                responseBean.setAnswers(answerBeans);
                            }
                        }
                    }
                    responseBeans.add(responseBean);
                }
                return gson.toJson(responseBeans);
            } else {
                log.info("Qus not found from summary");
                jsonobject.put("resCode", StaticValues.QUS_NOT_FOUND);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("006"));
                return jsonobject.toString();
            }
        } catch (Exception e) {
            try {
                log.error("Get Req Qus Exp : " + e.toString());
                jsonobject.put("resCode", StaticValues.SERVER_ERROR);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("003"));
            } catch (JSONException ex) {
                Logger.getLogger(QuestionController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return jsonobject.toString();
        }
    }

    public String ansUpdate(AnswerBean answerBean, String auth_key) {
        boolean ansRes = false;
        boolean result = false;
        boolean matchRes = false;
        int ansUpdateRes = 0;
        int resUpdateRes = 0;
        UsersBean usersBean = manageUsers.getUserByAuth_key(auth_key);
        if (usersBean != null) {
            result = validationController.timeValidation(usersBean);
        } else {
            try {
                log.info("Auth Key Failure ");
                jsonobject.put("resCode", StaticValues.AUTH_KEY_FAILURE);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("004"));
            } catch (JSONException ex) {
                Logger.getLogger(QuestionController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return jsonobject.toString();
        }
        if (result) {
            matchRes = manageAnswers.getAnsByQusAnsIntCode(answerBean.getQtnIntCode(), answerBean.getAnsIntCode());
            SummaryBean sb = manageSummary.getUserByuser_id(usersBean.getUser_id(), usersBean.getClient_id());
            if (matchRes) {
                ansUpdateRes = manageSummary.updateSummary(1, "C", answerBean.getDurationInSecs(), usersBean.getUser_id(), answerBean.getQtnIntCode());
                resUpdateRes = manageResults.updateResults(1, 1, 0, usersBean.getClient_id(), usersBean.getUser_id());
                log.info(" resUpdateRes for Correct : " + resUpdateRes);
            } else {
                ansUpdateRes = manageSummary.updateSummary(0, "W", answerBean.getDurationInSecs(), usersBean.getUser_id(), answerBean.getQtnIntCode());
                resUpdateRes = manageResults.updateResults(0, 0, 1, usersBean.getClient_id(), usersBean.getUser_id());
                log.info("resUpdateRes for wrong : " + resUpdateRes);;
            }
            if (ansUpdateRes > 0) {
                try {
                    jsonobject.put("resCode", StaticValues.SUCCESS);
                    jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("001"));
                } catch (JSONException ex) {
                    Logger.getLogger(QuestionController.class.getName()).log(Level.SEVERE, null, ex);
                }
                return jsonobject.toString();
            } else {
                try {
                    jsonobject.put("resCode", StaticValues.FAILURE);
                    jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("002"));
                } catch (JSONException ex) {
                    Logger.getLogger(QuestionController.class.getName()).log(Level.SEVERE, null, ex);
                }
                return jsonobject.toString();
            }
        } else {
            try {
                log.info("Time Expired ");
                jsonobject.put("resCode", StaticValues.TIME_EXPIRED);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("009"));
            } catch (JSONException ex) {
                Logger.getLogger(QuestionController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return jsonobject.toString();
        }
    }

}
