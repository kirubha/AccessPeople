/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.entities;

import java.util.Date;

/**
 *
 * @author suresh
 */
public class QusAnsBean {

    private int QtnIntCode;
    private int AnsIntCode;
    private Date created_date;
    private Date updated_date;

    public int getQtnIntCode() {
        return QtnIntCode;
    }

    public void setQtnIntCode(int QtnIntCode) {
        this.QtnIntCode = QtnIntCode;
    }

    public int getAnsIntCode() {
        return AnsIntCode;
    }

    public void setAnsIntCode(int AnsIntCode) {
        this.AnsIntCode = AnsIntCode;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Date getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Date updated_date) {
        this.updated_date = updated_date;
    }

}
