/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.entities;

import java.util.Date;

/**
 *
 * @author suresh
 */
public class ClientsBean {

    private String user_name;
    private String client_id;
    private String client_userid;
    private String client_password;
    private String client_ip;
    private String client_Mod_code;
    private boolean client_status;
    private int duration;
    private Date created_date;
    private Date updated_date;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_userid() {
        return client_userid;
    }

    public void setClient_userid(String client_userid) {
        this.client_userid = client_userid;
    }

    public String getClient_password() {
        return client_password;
    }

    public void setClient_password(String client_password) {
        this.client_password = client_password;
    }

    public String getClient_ip() {
        return client_ip;
    }

    public void setClient_ip(String client_ip) {
        this.client_ip = client_ip;
    }

    public String getClient_Mod_code() {
        return client_Mod_code;
    }

    public void setClient_Mod_code(String client_Mod_code) {
        this.client_Mod_code = client_Mod_code;
    }

    public boolean isClient_status() {
        return client_status;
    }

    public void setClient_status(boolean client_status) {
        this.client_status = client_status;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Date getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Date updated_date) {
        this.updated_date = updated_date;
    }

}
