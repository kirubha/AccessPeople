/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.entities;

import com.api.db.service.HBObject;
import java.util.Date;

/**
 *
 * @author suresh
 */
public class QuestionBean extends HBObject {

    private int QtnIntCode;
    private int AnsIntCode;
    private String ModuleCode;
    private String QtnType;
    private String TextQtn;
    private String ImgName;
    private String ImageBase64;
    private String VoiceQtn;
    private int QtnMark;
    private String QtnActiveStatus;
    private String Band;
    private int SubModuleCode;
    private String img_url;
    private int total_no_qus;
    private int request_qus;
    private Date created_date;
    private Date updated_date;

    public int getQtnIntCode() {
        return QtnIntCode;
    }

    public void setQtnIntCode(int QtnIntCode) {
        this.QtnIntCode = QtnIntCode;
    }

    public int getAnsIntCode() {
        return AnsIntCode;
    }

    public void setAnsIntCode(int AnsIntCode) {
        this.AnsIntCode = AnsIntCode;
    }

    public String getModuleCode() {
        return ModuleCode;
    }

    public void setModuleCode(String ModuleCode) {
        this.ModuleCode = ModuleCode;
    }

    public String getQtnType() {
        return QtnType;
    }

    public void setQtnType(String QtnType) {
        this.QtnType = QtnType;
    }

    public String getTextQtn() {
        return TextQtn;
    }

    public void setTextQtn(String TextQtn) {
        this.TextQtn = TextQtn;
    }

    public String getImgName() {
        return ImgName;
    }

    public void setImgName(String ImgName) {
        this.ImgName = ImgName;
    }

    public String getImageBase64() {
        return ImageBase64;
    }

    public void setImageBase64(String ImageBase64) {
        this.ImageBase64 = ImageBase64;
    }

    public String getVoiceQtn() {
        return VoiceQtn;
    }

    public void setVoiceQtn(String VoiceQtn) {
        this.VoiceQtn = VoiceQtn;
    }

    public int getQtnMark() {
        return QtnMark;
    }

    public void setQtnMark(int QtnMark) {
        this.QtnMark = QtnMark;
    }

    public String getQtnActiveStatus() {
        return QtnActiveStatus;
    }

    public void setQtnActiveStatus(String QtnActiveStatus) {
        this.QtnActiveStatus = QtnActiveStatus;
    }

    public String getBand() {
        return Band;
    }

    public void setBand(String Band) {
        this.Band = Band;
    }

    public int getSubModuleCode() {
        return SubModuleCode;
    }

    public void setSubModuleCode(int SubModuleCode) {
        this.SubModuleCode = SubModuleCode;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public int getTotal_no_qus() {
        return total_no_qus;
    }

    public void setTotal_no_qus(int total_no_qus) {
        this.total_no_qus = total_no_qus;
    }

    public int getRequest_qus() {
        return request_qus;
    }

    public void setRequest_qus(int request_qus) {
        this.request_qus = request_qus;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Date getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Date updated_date) {
        this.updated_date = updated_date;
    }

}
