/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.entities;

import com.api.db.service.HBObject;
import java.util.Date;

/**
 *
 * @author suresh
 */
public class AnswerBean extends HBObject {

    private int AnsIntCode;
    private int QtnIntCode;
    private String AnsType;
    private String TextAns;
    private String ImgAns;
    private int Marks;
    private int durationInSecs;
    private Date created_date;
    private Date updated_date;

    public int getAnsIntCode() {
        return AnsIntCode;
    }

    public void setAnsIntCode(int AnsIntCode) {
        this.AnsIntCode = AnsIntCode;
    }

    public int getQtnIntCode() {
        return QtnIntCode;
    }

    public void setQtnIntCode(int QtnIntCode) {
        this.QtnIntCode = QtnIntCode;
    }

    public String getAnsType() {
        return AnsType;
    }

    public void setAnsType(String AnsType) {
        this.AnsType = AnsType;
    }

    public String getTextAns() {
        return TextAns;
    }

    public void setTextAns(String TextAns) {
        this.TextAns = TextAns;
    }

    public String getImgAns() {
        return ImgAns;
    }

    public void setImgAns(String ImgAns) {
        this.ImgAns = ImgAns;
    }

    public int getMarks() {
        return Marks;
    }

    public void setMarks(int Marks) {
        this.Marks = Marks;
    }

    public int getDurationInSecs() {
        return durationInSecs;
    }

    public void setDurationInSecs(int durationInSecs) {
        this.durationInSecs = durationInSecs;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Date getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Date updated_date) {
        this.updated_date = updated_date;
    }

}
