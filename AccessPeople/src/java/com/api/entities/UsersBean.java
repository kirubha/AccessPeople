/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.entities;

import java.util.Date;

/**
 *
 * @author suresh
 */
public class UsersBean {

    private String client_id;
    private int user_id;
    private int validate_secs;
    private String auth_key;
    private String user_name;
    private char user_exam_status;
    private Date created_date;
    private Date updated_date;

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getValidate_secs() {
        return validate_secs;
    }

    public void setValidate_secs(int validate_secs) {
        this.validate_secs = validate_secs;
    }

    public String getAuth_key() {
        return auth_key;
    }

    public void setAuth_key(String auth_key) {
        this.auth_key = auth_key;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public char getUser_exam_status() {
        return user_exam_status;
    }

    public void setUser_exam_status(char user_exam_status) {
        this.user_exam_status = user_exam_status;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Date getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Date updated_date) {
        this.updated_date = updated_date;
    }

}
