/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.entities;

import java.util.ArrayList;

/**
 *
 * @author suresh
 */
public class ResponseBean {

    private QuestionBean questionBean;
    private ArrayList<AnswerBean> answers;

    public QuestionBean getQuestionBean() {
        return questionBean;
    }

    public void setQuestionBean(QuestionBean questionBean) {
        this.questionBean = questionBean;
    }

    public ArrayList<AnswerBean> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<AnswerBean> answers) {
        this.answers = answers;
    }

}
