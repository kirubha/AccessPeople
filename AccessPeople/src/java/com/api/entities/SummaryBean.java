/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.entities;

import com.api.db.service.HBObject;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author suresh
 */
public class SummaryBean extends HBObject implements Serializable {

    private String client_id;
    private Integer user_id;
    private Integer QtnIntCode;
    private String ModuleCode;
    private Integer exam_timetaken;
    private String qus_status;
    private Integer user_marks;
    private Integer AnsIntCode;
    private Date created_date;
    private Date updated_date;

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getQtnIntCode() {
        return QtnIntCode;
    }

    public void setQtnIntCode(Integer QtnIntCode) {
        this.QtnIntCode = QtnIntCode;
    }

    public String getModuleCode() {
        return ModuleCode;
    }

    public void setModuleCode(String ModuleCode) {
        this.ModuleCode = ModuleCode;
    }

    public Integer getExam_timetaken() {
        return exam_timetaken;
    }

    public void setExam_timetaken(Integer exam_timetaken) {
        this.exam_timetaken = exam_timetaken;
    }

    public String getQus_status() {
        return qus_status;
    }

    public void setQus_status(String qus_status) {
        this.qus_status = qus_status;
    }

    public Integer getUser_marks() {
        return user_marks;
    }

    public void setUser_marks(Integer user_marks) {
        this.user_marks = user_marks;
    }

    public Integer getAnsIntCode() {
        return AnsIntCode;
    }

    public void setAnsIntCode(Integer AnsIntCode) {
        this.AnsIntCode = AnsIntCode;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Date getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Date updated_date) {
        this.updated_date = updated_date;
    }

}
