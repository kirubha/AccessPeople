/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.entities;

/**
 *
 * @author suresh
 */
public class RequestResponse {

    private String txnCode;
    private String txnMsg;
    private String txnDate;
    private String resCode;
    private String resMsg;
    private String resDate;

    public RequestResponse() {
    }

    public RequestResponse(String txnCode, String txnMsg, String txnDate, String resCode, String resMsg, String resDate) {
        this.txnCode = txnCode;
        this.txnMsg = txnMsg;
        this.txnDate = txnDate;
        this.resCode = resCode;
        this.resMsg = resMsg;
        this.resDate = resDate;
    }

    public String getTxnCode() {
        return txnCode;
    }

    public void setTxnCode(String txnCode) {
        this.txnCode = txnCode;
    }

    public String getTxnMsg() {
        return txnMsg;
    }

    public void setTxnMsg(String txnMsg) {
        this.txnMsg = txnMsg;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getResCode() {
        return resCode;
    }

    public void setResCode(String resCode) {
        this.resCode = resCode;
    }

    public String getResMsg() {
        return resMsg;
    }

    public void setResMsg(String resMsg) {
        this.resMsg = resMsg;
    }

    public String getResDate() {
        return resDate;
    }

    public void setResDate(String resDate) {
        this.resDate = resDate;
    }

}
