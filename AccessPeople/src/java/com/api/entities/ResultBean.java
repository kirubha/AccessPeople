/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.entities;

import java.util.Date;

/**
 *
 * @author suresh
 */
public class ResultBean {

    private Integer seqno;
    private String client_id;
    private Integer user_id;
    private String ModuleCode;
    private int user_marks;
    private int correct_answers;
    private int wrong_answers;
    private int exam_timetaken;
    private int percentage;
    private Date created_date;
    private Date updated_date;

    public Integer getSeqno() {
        return seqno;
    }

    public void setSeqno(Integer seqno) {
        this.seqno = seqno;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getModuleCode() {
        return ModuleCode;
    }

    public void setModuleCode(String ModuleCode) {
        this.ModuleCode = ModuleCode;
    }

    public int getUser_marks() {
        return user_marks;
    }

    public void setUser_marks(int user_marks) {
        this.user_marks = user_marks;
    }

    public int getCorrect_answers() {
        return correct_answers;
    }

    public void setCorrect_answers(int correct_answers) {
        this.correct_answers = correct_answers;
    }

    public int getWrong_answers() {
        return wrong_answers;
    }

    public void setWrong_answers(int wrong_answers) {
        this.wrong_answers = wrong_answers;
    }

    public int getExam_timetaken() {
        return exam_timetaken;
    }

    public void setExam_timetaken(int exam_timetaken) {
        this.exam_timetaken = exam_timetaken;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Date getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Date updated_date) {
        this.updated_date = updated_date;
    }

}
