/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.db.service;

import com.api.entities.ClientsBean;
import com.api.entities.SummaryBean;
import com.api.utils.Dolog;
import com.api.utils.HibernateUtil;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author suresh
 */
public class ManageSummary {

    private static final Dolog log = new Dolog(ManageSummary.class);
    private static Gson gson = new Gson();
    ClientsBean clientsBean = new ClientsBean();
    SummaryBean summaryBean = new SummaryBean();

    public boolean batchInsert(ArrayList<SummaryBean> arrayList) {
        log.info("Batch Insert");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        boolean result = false;
        try {
            log.info("Summary Insert List : " + arrayList.size());
            txn = session.beginTransaction();
            for (int i = 0; i < arrayList.size(); i++) {
                log.info("Get One : " + arrayList.get(i).getQtnIntCode());
                session.save(arrayList.get(i));
                if (i % 50 == 0) {
                    session.flush();
                    session.clear();
                }
            }
            txn.commit();
            result = true;
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            log.info("Batch Insert Exp : " + e.toString());
            result = false;
        } finally {
            session.flush();
            session.close();
        }
        return result;
    }

    public int addSummary(SummaryBean summaryBean) throws Exception {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        int id = 0;

        try {
            txn = session.beginTransaction();
            id = (int) session.save(summaryBean);
            txn.commit();
        } catch (ConstraintViolationException ce) {
            if (txn != null) {
                txn.rollback();
            }
            return -1001;
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("ManageLogin exception" + e.toString());
            return -1002;
        } finally {
            session.close();
        }
        return id;
    }

    public List<?> listGroup() throws Exception {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> login = new ArrayList<>();
        try {

            txn = session.beginTransaction();
            login = (List<?>) session.createQuery("FROM com.api.entities.SummaryBean").list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return login;
    }

    public SummaryBean getClientsByClient_id(String client_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;

        try {

            hql = "FROM com.api.entities.SummaryBean where client_id=:client_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("client_id", client_id);
            if (query.list().size() == 1) {
                summaryBean = (SummaryBean) query.list().get(0);
                txn.commit();
            } else {
                return null;
            }
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("ManageLogin HB exception" + e.toString());
        } catch (Exception ex) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("ManageLogin exception" + ex.toString());
        } finally {
            session.close();
        }
        return summaryBean;
    }

    public int updateSummary(int user_marks, String qus_status, int duration, int user_id, int QtnIntCode) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;
        int result = 0;
        try {
            log.info("User id for update : " + user_id);
            txn = session.beginTransaction();
            hql = "update SummaryBean set user_marks=:user_marks,qus_status=:qus_status,exam_timetaken=:exam_timetaken where user_id=:user_id and QtnIntCode=:QtnIntCode";
            Query query = session.createQuery(hql);
            query.setParameter("user_marks", user_marks);
            query.setParameter("qus_status", qus_status);
            query.setParameter("exam_timetaken", duration);
            query.setParameter("user_id", user_id);
            query.setParameter("QtnIntCode", QtnIntCode);
            result = query.executeUpdate();
            log.info("Rows affected: " + result);
            txn.commit();
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            log.info("Msg Update Excep :" + e.toString());
        } finally {
            session.close();
        }
        return result;
    }

    public SummaryBean getUserByuser_id(int user_id, String client_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;

        try {
            log.info("User id : " + user_id);
            log.info("Client_id  : " + client_id);
            hql = "FROM com.api.entities.SummaryBean where user_id=:user_id and client_id=:client_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("user_id", user_id);
            query.setParameter("client_id", client_id);
            if (query.list().size() == 1) {
                summaryBean = (SummaryBean) query.list().get(0);
                txn.commit();
            } else {
                return null;
            }
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("SummaryBean HB exception" + e.toString());
        } catch (Exception ex) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("SummaryBean exception" + ex.toString());
        } finally {
            session.close();
        }
        return summaryBean;
    }

    public List<?> getUsersByClient_id(String client_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> questions = new ArrayList<>();
        try {

            String hql = "FROM com.api.entities.SummaryBean where client_id=:client_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("client_id", client_id);
            questions = query.list();
            if (!questions.isEmpty()) {
                questions = (List<?>) query.list();
            } else {
                return null;
            }
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return questions;
    }

    public List<?> getNotServedQus() {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> questions = new ArrayList<>();
        try {

            String hql = "FROM com.api.entities.SummaryBean where qus_status=:qus_status";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("qus_status", 'N');
            questions = (List<?>) query.list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return questions;
    }

    public List<?> getRandQusID(int totalCount, int user_id, String client_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> questions = new ArrayList<>();
        try {
            // For SQLSERVER you can use newid() for get random rows 
            String hql = "FROM com.api.entities.SummaryBean where qus_status=:qus_status and user_id=:user_id and client_id=:client_id ORDER BY rand()";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("qus_status", "N");
            query.setParameter("user_id", user_id);
            query.setParameter("client_id", client_id);
            query.setMaxResults(totalCount);
            questions = (List<?>) query.list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return questions;
    }

    public int getCountAnsStatus(String mod, int user_id) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;
        int obj = 0;
        try {
            hql = "select count(qus_status) FROM com.api.entities.SummaryBean where qus_status=:qus_status and user_id=:user_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("qus_status", mod);
            query.setParameter("user_id", user_id);
            obj = ((Long) query.uniqueResult()).intValue();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            log.info("Get Ans Mode Exp : " + e.toString());
        } finally {
            session.close();
        }
        return obj;
    }

    public static void main(String[] args) {
        ManageSummary manageSummary = new ManageSummary();
        try {
            System.out.println("" + manageSummary.getSumMarks(5));
        } catch (Exception ex) {
            Logger.getLogger(ManageSummary.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getSumMarks(int user_id) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;
        int obj = 0;
        try {
            hql = "select sum(user_marks) FROM com.api.entities.SummaryBean where user_id=:user_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("user_id", user_id);
            obj = ((Long) query.uniqueResult()).intValue();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("Sum Marks Exp : " + e.toString());
        } finally {
            session.close();
        }
        return obj;
    }

    public List<?> getCountCrtAns(int user_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> questions = new ArrayList<>();
        try {

            String hql = "FROM com.api.entities.SummaryBean where user_id=:user_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("user_id", user_id);
            questions = (List<?>) query.list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return questions;
    }

}
