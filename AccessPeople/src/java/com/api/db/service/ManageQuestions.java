/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.db.service;

import com.api.entities.QuestionBean;
import com.api.utils.Dolog;
import com.api.utils.HibernateUtil;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author suresh
 */
public class ManageQuestions {

    private static final Dolog log = new Dolog(ManageQuestions.class);
    private static Gson gson = new Gson();
    QuestionBean questionBean = new QuestionBean();

    public String addQuestion(QuestionBean questionBean) throws Exception {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String id = null;

        try {
            txn = session.beginTransaction();
            id = (String) session.save(questionBean);
            txn.commit();
        } catch (ConstraintViolationException ce) {
            if (txn != null) {
                txn.rollback();
            }
            return "-1001";
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("ManageLogin exception" + e.toString());
            return "-1002";
        } finally {
            session.close();
        }
        return id;
    }

    public List<?> listGroup() throws Exception {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> login = new ArrayList<>();
        try {

            txn = session.beginTransaction();
            login = (List<?>) session.createQuery("FROM com.api.entities.QuestionBean").list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return login;
    }

    public QuestionBean getQuestionsByModuleCode(String code) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;

        try {

            hql = "FROM com.api.entities.QuestionBean where ModuleCode=:ModuleCode";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("ModuleCode", code);
            if (query.list().size() == 1) {
                questionBean = (QuestionBean) query.list().get(0);
                txn.commit();
            } else {
                return null;
            }
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("ManageLogin HB exception" + e.toString());
        } catch (Exception ex) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("ManageLogin exception" + ex.toString());
        } finally {
            session.close();
        }
        return questionBean;
    }

    public List<?> getQuestionsByQusIntCode(int QtnIntCode) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> questions = new ArrayList<>();
        try {

            String hql = "FROM com.api.entities.QuestionBean where QtnIntCode=:QtnIntCode";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("QtnIntCode", QtnIntCode);
            questions = (List<?>) query.list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return questions;
    }

    public List<?> getQuestionsByModCode(String modulecode) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> questions = new ArrayList<>();
        try {

            String hql = "FROM com.api.entities.QuestionBean where ModuleCode=:ModuleCode";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("ModuleCode", modulecode);
            questions = (List<?>) query.list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return questions;
    }

    public List<?> getQuesByModCodeWithTotalQus(String modulecode, int totalCount) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> questions = new ArrayList<>();
        try {
            // For SQLSERVER you can use newid() for get random rows 
            String hql = "FROM com.api.entities.QuestionBean where ModuleCode=:ModuleCode ORDER BY rand()";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("ModuleCode", modulecode);
            query.setFirstResult(0);
            query.setMaxResults(totalCount);
            questions = (List<?>) query.list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            log.info("Get Count of Qus Exp : " + e.toString());
        } finally {
            session.close();
        }
        return questions;
    }

}
