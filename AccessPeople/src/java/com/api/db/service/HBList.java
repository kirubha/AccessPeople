/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.db.service;

import com.api.entities.SummaryBean;
import com.api.utils.HibernateUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author THIYAGARAAJ
 */
public class HBList<T> {

//    private static final DoLog log = new DoLog(HBList.class);
    public static String UNIQUE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static boolean getJSON(String[] requiredString) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private List<Object> list;
    private final Class<T> type;
    private String query;
    private HashMap<String, Object> params = new HashMap<>();

    public HBList(Class<T> type) {
        list = new ArrayList<>();
        this.type = type;
    }

    public List<?> getList(HashMap<String, Object> params) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        list.clear();
        //List<?> users = new ArrayList<>();
        try {
            txn = session.beginTransaction();
            System.out.println("Data : " + type.getCanonicalName());

            Iterator it = params.entrySet().iterator();
            int length = 0;
            //Query query = session.createQuery(hql);
            String hql = "";
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                //System.out.println(pair.getKey() + " = " + pair.getValue());
                hql = hql + pair.getKey() + "=:" + pair.getKey();
                if (length == params.size() - 1) {
                    hql = "FROM " + type.getCanonicalName() + " where " + hql;
                } else {
                    hql = hql + " and ";
                }
                length++;
                //it.remove(); // avoids a ConcurrentModificationException
            }
            System.out.println("HQL : " + hql);

            Query query = session.createQuery(hql);

            Iterator its = params.entrySet().iterator();
            while (its.hasNext()) {
                Map.Entry pair = (Map.Entry) its.next();
                System.out.println(pair.getKey() + " = " + pair.getValue());
                hql = hql + pair.getKey() + "=:" + pair.getKey();
                query.setParameter(pair.getKey().toString(), pair.getValue());
            }

            list = (List<Object>) (List<?>) query.list();
            //list = (List<Object>) (List<?>) session.createQuery("FROM " + type.getCanonicalName()).list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return list;
    }

    public Object getObject(String hql, HashMap<String, Object> params) throws Exception {
        List<?> objectList = new ArrayList<>();
        objectList = getList(hql, params);
        if (objectList.size() > 0) {
            return objectList.get(0);
        } else {
            return null;
        }
    }

    public List<?> getList(String hql, HashMap<String, Object> params) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        list.clear();
        //List<?> users = new ArrayList<>();
        try {
            txn = session.beginTransaction();
            System.out.println("Data : " + type.getCanonicalName());

            hql = " FROM " + type.getCanonicalName() + " where " + hql;

            Iterator it = params.entrySet().iterator();
            //int length = 0;            
            System.out.println("HQL : " + hql);
            Query query = session.createQuery(hql);

            Iterator its = params.entrySet().iterator();
            while (its.hasNext()) {
                Map.Entry pair = (Map.Entry) its.next();
                System.out.println(pair.getKey() + " = " + pair.getValue());
                hql = hql + pair.getKey() + "=:" + pair.getKey();
                query.setParameter(pair.getKey().toString(), pair.getValue());
                System.out.println("HASH MAP KEY : " + pair.getKey().toString());
                System.out.println("HASH MAP Value : " + pair.getValue());

            }
            list = (List<Object>) (List<?>) query.list();
            System.out.println("get List :" + list.size());
            //list = (List<Object>) (List<?>) session.createQuery("FROM " + type.getCanonicalName()).list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return list;
    }

    public Long getCount(HashMap<String, Object> params) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        list.clear();
        Long count = new Long(0);
        //List<?> users = new ArrayList<>();
        try {
            txn = session.beginTransaction();
            System.out.println("Data : " + type.getCanonicalName());

            //Query query = session.createQuery(hql);
            String hql = "";
            if (params != null) {
                Iterator it = params.entrySet().iterator();
                int length = 0;

                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    //System.out.println(pair.getKey() + " = " + pair.getValue());
                    hql = hql + pair.getKey() + "=:" + pair.getKey();
                    if (length == params.size() - 1) {
                        hql = "select count(*) FROM " + type.getCanonicalName() + " where " + hql;
                    } else {
                        hql = hql + " and ";
                    }
                    length++;
                    //it.remove(); // avoids a ConcurrentModificationException
                }
            } else {
                hql = "select count(*) FROM " + type.getCanonicalName();
            }
            System.out.println("HQL : " + hql);

            Query dataQuery = session.createQuery(hql);

            if (params != null) {
                Iterator its = params.entrySet().iterator();
                while (its.hasNext()) {
                    Map.Entry pair = (Map.Entry) its.next();
                    System.out.println(pair.getKey() + " = " + pair.getValue());
                    hql = hql + pair.getKey() + "=:" + pair.getKey();
                    dataQuery.setParameter(pair.getKey().toString(), pair.getValue());
                }
            }
            count = (Long) dataQuery.uniqueResult();
            //list = (List<Object>) (List<?>) session.createQuery("FROM " + type.getCanonicalName()).list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return count;
    }

    public static void main(String[] args) {
        HBList<SummaryBean> hBList = new HBList(SummaryBean.class);
        double data = 0;
        try {
            HashMap<String, Object> hashmap = new HashMap();
            hashmap.put("user_id", 5);
            hashmap.put("client_id","0388");
            data = hBList.getSum("marks", hashmap);
            System.out.println("DATA DOUBLE :" + data);
        } catch (Exception ex) {
            System.out.println("usersdata ord_amt : " + ex.toString());
        }
    }

    public Double getSum(String columnName, HashMap<String, Object> params) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        list.clear();
        Double count = new Double(0);
        //List<?> users = new ArrayList<>();
        try {
            txn = session.beginTransaction();
            System.out.println("Data : " + type.getCanonicalName());

            //Query query = session.createQuery(hql);
            String hql = "";
            if (params != null) {
                Iterator it = params.entrySet().iterator();
                int length = 0;

                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    //System.out.println(pair.getKey() + " = " + pair.getValue());
                    hql = hql + pair.getKey() + "=:" + pair.getKey();
                    if (length == params.size() - 1) {
                        hql = "select sum(" + columnName + ") FROM " + type.getCanonicalName() + " where " + hql;
                    } else {
                        hql = hql + " and ";
                    }
                    length++;
                    //it.remove(); // avoids a ConcurrentModificationException
                }
            } else {
                hql = "select sum(" + columnName + ") FROM " + type.getCanonicalName();
            }
            System.out.println("HQL : " + hql);

            Query dataQuery = session.createQuery(hql);

            if (params != null) {
                Iterator its = params.entrySet().iterator();
                while (its.hasNext()) {
                    Map.Entry pair = (Map.Entry) its.next();
                    System.out.println(pair.getKey() + " = " + pair.getValue());
                    hql = hql + pair.getKey() + "=:" + pair.getKey();
                    dataQuery.setParameter(pair.getKey().toString(), pair.getValue());
                }
            }
            count = (Double) dataQuery.uniqueResult();
            //list = (List<Object>) (List<?>) session.createQuery("FROM " + type.getCanonicalName()).list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return count;
    }

    public List<?> getList(String hql) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        list.clear();
        try {
            txn = session.beginTransaction();
            Query query = session.createQuery("FROM " + type.getCanonicalName() + " where " + hql);
            list = (List<Object>) (List<?>) query.list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return list;
    }

    public List<?> getList() throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        list.clear();
        //List<?> users = new ArrayList<>();
        try {
            txn = session.beginTransaction();
            System.out.println("Data : " + type.getCanonicalName());
            list = (List<Object>) (List<?>) session.createQuery("FROM " + type.getCanonicalName()).list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return list;
    }

    public String getJSON() throws Exception {
        GsonBuilder b = new GsonBuilder().disableHtmlEscaping();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        b.setDateFormat(UNIQUE_DATE_FORMAT);

        Gson gson = b.create();
        String data = gson.toJson(list);
        return data;
    }

    public static String getJSON(List<?> new_list) throws Exception {
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        b.setDateFormat(UNIQUE_DATE_FORMAT);

        Gson gson = b.create();
        String data = gson.toJson(new_list);
        return data;
    }

    public static String getJSONHtmlEscaping(List<?> new_list) throws Exception {
        GsonBuilder b = new GsonBuilder().disableHtmlEscaping();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        b.setDateFormat(UNIQUE_DATE_FORMAT);

        Gson gson = b.create();
        String data = gson.toJson(new_list);
        return data;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void clearParams() {
        params.clear();
    }

    public void setParam(String key, String value) {
        params.put(key, value);
    }
}
