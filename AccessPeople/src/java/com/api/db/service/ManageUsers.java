/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.db.service;

import com.api.entities.ClientsBean;
import com.api.entities.UsersBean;
import com.api.utils.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author suresh
 */
public class ManageUsers {

    UsersBean usersBean = new UsersBean();

    public int addUser(UsersBean usersBean) throws Exception {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        int id = 0;

        try {
            txn = session.beginTransaction();
            id = (int) session.save(usersBean);
            txn.commit();
        } catch (ConstraintViolationException ce) {
            if (txn != null) {
                txn.rollback();
            }
            return -1001;
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            return -1002;
        } finally {
            session.close();
        }
        return id;
    }

    public UsersBean getUserByClntwithUsr_id(String client_id, int user_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;

        try {

            hql = "FROM com.api.entities.UsersBean where client_id=:client_id and user_id=:user_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("client_id", client_id);
            query.setParameter("user_id", user_id);
            if (query.list().size() == 1) {
                usersBean = (UsersBean) query.list().get(0);
                txn.commit();
            } else {
                return null;
            }
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
        } catch (Exception ex) {
            if (txn != null) {
                txn.rollback();
            }
        } finally {
            session.close();
        }
        return usersBean;
    }

    public UsersBean getUserByClient_id(String client_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;

        try {

            hql = "FROM com.api.entities.UsersBean where client_id=:client_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("client_id", client_id);
            if (query.list().size() == 1) {
                usersBean = (UsersBean) query.list().get(0);
                txn.commit();
            } else {
                return null;
            }
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
        } catch (Exception ex) {
            if (txn != null) {
                txn.rollback();
            }
        } finally {
            session.close();
        }
        return usersBean;
    }

    public UsersBean getUserByUser_id(int user_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;

        try {

            hql = "FROM com.api.entities.UsersBean where user_id=:user_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("user_id", user_id);
            if (query.list().size() == 1) {
                usersBean = (UsersBean) query.list().get(0);
                txn.commit();
            } else {
                return null;
            }
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
        } catch (Exception ex) {
            if (txn != null) {
                txn.rollback();
            }
        } finally {
            session.close();
        }
        return usersBean;
    }

    public UsersBean getUserByAuth_key(String auth_key) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;

        try {

            hql = "FROM com.api.entities.UsersBean where auth_key=:auth_key";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("auth_key", auth_key);
            if (query.list().size() == 1) {
                usersBean = (UsersBean) query.list().get(0);
                txn.commit();
            } else {
                return null;
            }
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
        } catch (Exception ex) {
            if (txn != null) {
                txn.rollback();
            }
        } finally {
            session.close();
        }
        return usersBean;
    }

}
