/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.db.service;

import com.api.entities.ResultBean;
import com.api.entities.SummaryBean;
import com.api.utils.Dolog;
import com.api.utils.HibernateUtil;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author suresh
 */
public class ManageResults {

    private static final Dolog log = new Dolog(ManageResults.class);
    private static Gson gson = new Gson();
    ResultBean resultBean = new ResultBean();

    public boolean batchInsert(ArrayList<ResultBean> arrayList) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = session.beginTransaction();
        boolean result = false;
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                session.save(arrayList.get(i));
                if (i % 50 == 0) {
                    session.flush();
                    session.clear();
                }
            }
            txn.commit();
            result = true;
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
        } finally {
            session.flush();
            session.close();
        }
        return result;
    }

    public int addResult(ResultBean resultBean) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        boolean result = false;
        int id = 0;

        try {
            txn = session.beginTransaction();
            id = (int) session.save(resultBean);
            txn.commit();
            result = true;
        } catch (ConstraintViolationException ce) {
            if (txn != null) {
                txn.rollback();
            }
            result = false;
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("Add Result to DB exception" + e.toString());
            result = false;
        } finally {
            session.close();
        }
        return id;
    }

    public List<?> listGroup() throws Exception {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> login = new ArrayList<>();
        try {

            txn = session.beginTransaction();
            login = (List<?>) session.createQuery("FROM com.api.entities.ResultBean").list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return login;
    }

    public int updateResults(int user_marks, int correct, int wrong, String client_id, int user_id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;
        int result = 0;
        try {
            txn = session.beginTransaction();
            hql = "update ResultBean set user_marks = user_marks+:user_marks,correct_answers = correct_answers+:correct_answers,wrong_answers=wrong_answers+:wrong_answers where user_id=:user_id and client_id=:client_id";
            Query query = session.createQuery(hql);
            query.setParameter("user_marks", user_marks);
            query.setParameter("correct_answers", correct);
            query.setParameter("wrong_answers", wrong);
            query.setParameter("user_id", user_id);
            query.setParameter("client_id", client_id);
//            query.setParameter("ModuleCode", ModuleCode);
            result = query.executeUpdate();
            log.info("Rows affected: " + result);
            txn.commit();
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            log.info("Msg Update Excep :" + e.toString());
        } finally {
            session.close();
        }
        return result;
    }

    public List<?> getUsersByClient_id(String client_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> results = new ArrayList<>();
        try {

            String hql = "FROM com.api.entities.ResultBean where client_id=:client_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("client_id", client_id);
            results = query.list();
            if (!results.isEmpty()) {
                results = (List<?>) query.list();
            } else {
                return null;
            }
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return results;
    }

    public ResultBean getUserByuser_id(int user_id, String client_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;
        ResultBean resultBean = new ResultBean();

        try {
            log.info("User id : " + user_id);
            log.info("Client_id  : " + client_id);
            hql = "FROM com.api.entities.ResultBean where user_id=:user_id and client_id=:client_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("user_id", user_id);
            query.setParameter("client_id", client_id);
            if (query.list().size() == 1) {
                resultBean = (ResultBean) query.list().get(0);
                txn.commit();
            } else {
                return null;
            }
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("resultBean HB exception" + e.toString());
        } catch (Exception ex) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("resultBean exception" + ex.toString());
        } finally {
            session.close();
        }
        return resultBean;
    }

    public List<?> getResultsListByUser_id(int user_id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> questions = new ArrayList<>();
        try {

            String hql = "FROM com.api.entities.ResultBean where user_id=:user_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("user_id", user_id);
            questions = (List<?>) query.list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return questions;
    }

}
