/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.db.service;


import com.api.utils.HibernateUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.AliasToEntityMapResultTransformer;

/**
 *
 * @author THIYAGARAAJ
 */
public class HBClass {

//    private static final DoLog log = new DoLog(HBClass.class);
    public static String UNIQUE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static List<Object[]> fetch(String hql) {
        return fetch(hql, null);
    }

    public static List<Object[]> fetch(String hql, HashMap<String, Object> params) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        List<Object[]> obj = null;

        try {
            Query query = session.createQuery(hql);
            if (params != null) {
                Iterator its = params.entrySet().iterator();
                while (its.hasNext()) {
                    Map.Entry pair = (Map.Entry) its.next();
                    System.out.println(pair.getKey() + " = " + pair.getValue());
                    hql = hql + pair.getKey() + "=:" + pair.getKey();
                    query.setParameter(pair.getKey().toString(), pair.getValue());
                }
            }

            obj = query.list();
            System.out.println(" " + obj.size());//0

        } catch (HibernateException e) {
            if (tx != null) {
                e.printStackTrace();
            }
        } finally {
            session.flush();
            session.close();
        }
        return obj;
    }

    public static List<Map<String, Object>> fetchSQL(String hql) {
        return fetchSQL(hql, null);
    }

    public static List<Map<String, Object>> fetchSQL(String sql, HashMap<String, Object> params) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        List<Map<String, Object>> obj = null;

        try {
            //Query query = session.createQuery(hql);
            SQLQuery query = session.createSQLQuery(sql);
            query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
            if (params != null) {
                Iterator its = params.entrySet().iterator();
                while (its.hasNext()) {
                    Map.Entry pair = (Map.Entry) its.next();
                    System.out.println(pair.getKey() + " = " + pair.getValue());
                    sql = sql + pair.getKey() + "=:" + pair.getKey();
                    query.setParameter(pair.getKey().toString(), pair.getValue());
                }
            }

            //obj = query.list();
            System.out.println("Query :" + query);
            obj = query.list();
//            /System.out.println(" " + obj.size());//0

        } catch (HibernateException e) {
            if (tx != null) {
                e.printStackTrace();
            }
        } finally {
            session.flush();
            session.close();
        }
        return obj;
    }

    public static boolean dmlSQL(String hql) {
        return dmlSQL(hql, null);
    }

    public static String listToString(ArrayList<String> stringList) {
        String queryparam = "";
        if (stringList.size() > 0) {
            for (int i = 0; i < stringList.size(); i++) {
                if (stringList.size() - 1 == i) {
                    queryparam = queryparam + StringManager.DQ(stringList.get(i).toString());
                } else {
                    queryparam = queryparam + StringManager.DQ(stringList.get(i).toString()) + ",";
                }
            }
        } else {
            queryparam = "" + 0;
        }
        return queryparam;
    }

    public static boolean dmlSQL(String sql, HashMap<String, Object> params) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        boolean obj = false;

        try {
            //Query query = session.createQuery(hql);
            SQLQuery query = session.createSQLQuery(sql);
            query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
            if (params != null) {
                Iterator its = params.entrySet().iterator();
                while (its.hasNext()) {
                    Map.Entry pair = (Map.Entry) its.next();
                    System.out.println(pair.getKey() + " = " + pair.getValue());
                    sql = sql + pair.getKey() + "=:" + pair.getKey();
                    if (pair.getValue() instanceof ArrayList) {
                        query.setParameterList((String) pair.getKey(), (ArrayList) pair.getValue());
                    } else {
                        query.setParameter((String) pair.getKey(), pair.getValue());
                    }
                    //query.setParameter(pair.getKey().toString(), pair.getValue());
                }
            }

            //obj = query.list();
            //obj=query.list();
            query.executeUpdate();
            obj = true;
            tx.commit();
//            /System.out.println(" " + obj.size());//0

        } catch (HibernateException e) {
            if (tx != null) {
                e.printStackTrace();
            }
        } finally {
            session.flush();
            session.close();
        }
        return obj;
    }

//     public List<?> runNaiveQuery(Map params, String hsql) throws Exception {
//        List<?> returnObjects = new ArrayList<>();
//        try {
//            SQLQuery query = session.createSQLQuery(hsql);
//            setParams(query, params);
//            System.out.println("QUERY :" + query.toString());
//            returnObjects = query.list();
//        } catch (HibernateException e) {
//            e.printStackTrace();
//        }
//        return returnObjects;
//    }
    // Sample Usage
    public static void main(String[] args) throws Exception {
        String hql = "SELECT userId,userName,roleId FROM Users where roleId=:roleId";

        HashMap<String, Object> data = new HashMap<>();
        data.put("roleId", 2);
        List<Object[]> obj = HBClass.fetch(hql, data);
        System.out.println("Data Size:" + obj.size());
        System.out.println("Data Size:" + HBList.getJSON(obj));
        HibernateUtil.close();
    }
}
