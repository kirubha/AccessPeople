/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.db.service;

import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;


/**
 *
 * @author YSEUSER
 */
public class StringManager {

    public static String DQ(String data) {
        return "\"" + data + "\"";
    }

    public static String[] removeDuplicates(String[] strArr) {
        //strArr = {"one","two","three","four","four","five"};
        //convert string array to list
        List<String> tmpList = Arrays.asList(strArr);
        //create a treeset with the list, which eliminates duplicates
        TreeSet<String> unique = new TreeSet<String>(tmpList);
        //log.info(unique);
        String[] result = unique.toArray(new String[unique.size()]);
        return result;
    }

    public static String removeWhiteSpaces(String data) {
        return data.replaceAll("\\s+", "");
    }

    public static String removeDuplicates(String data) {
        data = removeWhiteSpaces(data);
        String[] inArr = data.split(",");
        String[] strArr = removeDuplicates(inArr);
        data = Arrays.toString(strArr);
        return data;
    }

    public static String removeAlphaBets(String data) {
        return data.replaceAll("[^\\d.]", "");
    }

    public static String removeNumbers(String data) {
        return data.replaceAll("[^A-Za-z]", "");
    }

//    public static String base64UrlDecode(String input) {
//        String result = null;
//        Base64 decoder = new Base64(true);
//        byte[] decodedBytes = decoder.decode(input);
//        result = new String(decodedBytes);
//        return result;
//    }
}
