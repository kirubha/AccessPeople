/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.db.service;

import com.api.entities.ClientsBean;
import com.api.utils.Dolog;
import com.api.utils.HibernateUtil;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author suresh
 */
public class ManageClients {

    private static final Dolog log = new Dolog(ManageClients.class);
    private static Gson gson = new Gson();
    ClientsBean clientsBean = new ClientsBean();

    public String addLogin(ClientsBean clientsBean) throws Exception {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String id = null;

        try {
            txn = session.beginTransaction();
            id = (String) session.save(clientsBean);
            txn.commit();
        } catch (ConstraintViolationException ce) {
            if (txn != null) {
                txn.rollback();
            }
            return "-1001";
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("ManageLogin exception" + e.toString());
            return "-1002";
        } finally {
            session.close();
        }
        return id;
    }

    public List<?> listGroup() throws Exception {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> login = new ArrayList<>();
        try {

            txn = session.beginTransaction();
            login = (List<?>) session.createQuery("FROM com.api.entities.ClientsBean").list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return login;
    }

    public ClientsBean getUserByClientId(String client_id) throws Exception {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> users = new ArrayList<>();
        ClientsBean clients = new ClientsBean();
        try {

            String hql = "FROM com.api.entities.ClientsBean where client_id=:client_id";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("client_id", client_id);
            users = query.list();
            log.info("Users Size : " + users.size());
            if (!users.isEmpty()) {
                clients = (ClientsBean) query.list().get(0);
            } else {
                return null;
            }
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return clients;
    }

    public static void main(String[] args) throws Exception {
        ManageClients manageClients = new ManageClients();
        ClientsBean clientsBean = manageClients.getUserByClientIP("127.0.0.1");
        System.out.println(""+clientsBean.getClient_ip());
        System.out.println(""+clientsBean.getClient_id());
    }
    public ClientsBean getUserByClientIP(String client_ips) throws Exception {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> users = new ArrayList<>();
        ClientsBean clients = new ClientsBean();
        try {
            String hql = "FROM com.api.entities.ClientsBean where client_ips like ?";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter(0, "%" + client_ips + "%");
            users = query.list();
            log.info("Users Size : " + users.size());
            if (!users.isEmpty()) {
                clients = (ClientsBean) query.list().get(0);
            } else {
                return null;
            }
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return clients;
    }

//    public static void main(String[] args) {
//        ManageClients clients = new ManageClients();
//        ClientsBean bean = clients.validateClient("0388");
//        System.out.println("" + bean.getClient_id());
//    }
    public ClientsBean validateClient(String client_id, String client_password) {
        System.out.println("GET Client id : " + client_id);
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        String hql = null;
        ClientsBean clientsBean = new ClientsBean();

        try {

            hql = "FROM com.api.entities.ClientsBean where client_id=:client_id and client_password=:client_password";
            txn = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("client_id", client_id);
            query.setParameter("client_password", client_password);
            log.info("query size : " + query.list().size());
            if (query.list().size() == 1) {
                clientsBean = (ClientsBean) query.list().get(0);
                txn.commit();
            } else {
                return null;
            }
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("ManageClients HB exception" + e.toString());
        } catch (Exception ex) {
            if (txn != null) {
                txn.rollback();
            }
            log.error("ManageClients exception" + ex.toString());
        } finally {
            session.close();
        }
        return clientsBean;
    }
}
