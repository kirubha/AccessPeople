/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.db.service;

import com.api.utils.HibernateUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

/**
 *
 * @author THIYAGARAAJ
 */


public class HBObject {

    public HBObject() {

    }

    public Object getMaxId(String baseColumn) {
        Transaction txn = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Object object = null;
        try {
            txn = session.beginTransaction();
            Criteria c = session.createCriteria(this.getClass());
            c.addOrder(Order.desc(baseColumn));
            c.setMaxResults(1);
            object = c.uniqueResult();
            //maxId = ((Language) c.uniqueResult()).getLanguageId();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return object;
    }

//    public String getNextId(String currentMaxID) {
//        System.out.println("currentMaxID : " + currentMaxID);
//        String oldValue = StringManager.removeAlphaBets(currentMaxID);
//        Integer casex = new Integer(StringManager.removeAlphaBets(currentMaxID));
//        String value = String.format("%0" + oldValue.length() + "d", (casex + 1));
//        String nextID = StringManager.removeNumbers(currentMaxID) + value;
//        System.out.println("nextID : " + nextID);
//        return nextID;
//    }

    public List<?> list() throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        List<?> users = new ArrayList<>();
        try {
            txn = session.beginTransaction();
            System.out.println("Data : " + this.getClass().getCanonicalName());
            users = (List<?>) session.createQuery("FROM " + this.getClass().getCanonicalName()).list();
            txn.commit();
        } catch (HibernateException e) {
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return users;
    }

   

    public boolean dataSave() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        boolean result = false;
        try {
            txn = session.beginTransaction();
            session.saveOrUpdate(this);
            txn.commit();
            result = true;
        } catch (HibernateException e) {
            result = false;
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return result;
    }

    public boolean dataInsert() throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        boolean result = false;
        try {
            txn = session.beginTransaction();
            session.save(this);
            txn.commit();
            result = true;
        } catch (HibernateException e) {
            result = false;
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return result;
    }

    public boolean delete() throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction txn = null;
        boolean result = false;
        try {
            txn = session.beginTransaction();
            //Users deleteUser = (Users) session.get(Users.class, userId);
            session.delete(this);
            txn.commit();
            result = true;
        } catch (HibernateException e) {
            result = false;
            if (txn != null) {
                txn.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return result;
    }

    public <T> T getObject(String json) {
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
//        b.setDateFormat(STATIC_MANAGER.UNIQUE_DATE_FORMAT);
        b.setDateFormat("yyyy-MM-dd HH:mm:ss");
        //b.setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        //b.setDateFormat(DateFormat.FULL, DateFormat.FULL);

        Gson gson = b.create();
        return (T) gson.fromJson(json, this.getClass());
    }

    public <T> T getObject2(String json) {
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
//        b.setDateFormat(STATIC_MANAGER.ANOTHERUNIQUE_DATE_FORMAT);
        b.setDateFormat("yyyy-MM-dd HH:mm:ss");
        //b.setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        //b.setDateFormat(DateFormat.FULL, DateFormat.FULL);

        Gson gson = b.create();
        return (T) gson.fromJson(json, this.getClass());
    }

    public String getJSON() throws Exception {
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
//        b.setDateFormat(STATIC_MANAGER.UNIQUE_DATE_FORMAT);

        b.setDateFormat("yyyy-MM-dd HH:mm:ss");
        //b.setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        //b.setDateFormat(DateFormat.FULL, DateFormat.FULL);
        Gson gson = b.create();

        String data = gson.toJson(this);
        //System.out.println("GetJSON :"+data);
        return data;
    }
}
