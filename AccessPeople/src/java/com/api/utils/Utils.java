/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.utils;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author suresh
 */
public class Utils {

    private static final Dolog log = new Dolog(Utils.class);
    JSONObject jsonobject = new JSONObject();

    public String genAuth_key() {
        SecureRandom random = new SecureRandom();
        String key = new BigInteger(130, random).toString(32);
        log.info(" session key id : " + key);
        return key;
    }

    public static float getPercentage(int n, int total) {
        float proportion = ((float) n) / ((float) total);
        return proportion * 100;
    }

    public String convertDatetoString(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date today = Calendar.getInstance().getTime();
        String reportDate = df.format(date);
        return reportDate;
    }

    public static String getCurrentTimeStampFormat(Date date) {
        String Str = null;
        try {
            DateFormat f1 = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//            Date date = new Date();
            f1.setTimeZone(TimeZone.getTimeZone("IST"));
            Date d = f1.parse(date.toString());

            DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Str = f2.format(d).toLowerCase();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Str;
    }

    public Timestamp genTimeStamp() {
        java.util.Date date = new java.util.Date();
        log.info("DATE TIME : " + date.getTime());
        return new Timestamp(date.getTime());
    }

    public String getTimeStamp() {
        java.util.Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        SimpleDateFormat noMilliSecondsFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return noMilliSecondsFormatter.format(timestamp);
    }

    public Date stringToDateWithTime(String value) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
        Date startDate;
        startDate = df.parse(value);
        String newDateString = df.format(startDate);
        return startDate;
    }

    public static Date stringToDateOnly(String value) throws ParseException {
        log.info("Date :" + value);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate;
        startDate = df.parse(value);
        String newDateString = df.format(startDate);
        return startDate;
    }

    public static Date convertStrIntoDate(String dateString) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = fmt.parse(dateString);
        } catch (Exception e) {
            log.info(" Exp : " + e.toString());
        }

        return date;
    }

    public static String getTime(Date date) {
        Timestamp stamp = new Timestamp(System.currentTimeMillis());
//        Date date = new Date(stamp.getTime());
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
//        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    public int genMinits() {
        Calendar now = Calendar.getInstance();
        log.info("Minite : " + now.get(Calendar.MINUTE));
        return now.get(Calendar.MINUTE);
    }

    public static long diffTimeStamp(String a, String b) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
        Date firstParsedDate = dateFormat.parse(a);
        Date secondParsedDate = dateFormat.parse(b);
        long diff = secondParsedDate.getTime() - firstParsedDate.getTime();
        return diff;
    }

    public static Date parseTimeStmpToDateonly(Date date) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
//        Date date = new Date();
        String str = dateFormat.format(date);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        log.info("Parse date : " + df.parse(str));
        return df.parse(str);
    }

    public static String getCurrentDateFormat(Date date) {
        String Str = null;
        try {
            DateFormat f1 = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//            Date date = new Date();
            f1.setTimeZone(TimeZone.getTimeZone("IST"));
            Date d = f1.parse(date.toString());

            DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd");
            Str = f2.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Str;
    }

    public static void main(String[] args) throws ParseException {
        Utils utils = new Utils();
        String a = "2012-01-14 09:00:00";
        String b = "2012-01-14 10:00:00";
        System.out.println("" + utils.diffMinits(a, b));

    }

    public long diffMinits(String a, String b) throws ParseException {
        long diffMinutes = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date firstParsedDate = dateFormat.parse(a);
        Date secondParsedDate = dateFormat.parse(b);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = sdf.parse(getCurrentDateFormat(firstParsedDate));
        Date date2 = sdf.parse(getCurrentDateFormat(secondParsedDate));
        log.info("Date Parse: " + date1.equals(date2));
        if (date1.equals(date2)) {
            long diff = firstParsedDate.getTime() - secondParsedDate.getTime();
            long diffHours = diff / (60 * 60 * 1000) % 24;
            if (diffHours == 0) {
                diffMinutes = diff / (60 * 1000) % 60;
            }
            long diffDays = diff / (24 * 60 * 60 * 1000);
            long diffSeconds = diff / 1000 % 60;
            System.out.println(diffMinutes + " minutes, ");
            System.out.println(diffDays + " days, ");
            System.out.println(diffHours + " hours, ");
            System.out.println(diffSeconds + " seconds.");

        }
        return diffMinutes;
    }

//    public static void main(String[] args) throws ParseException {
//        Calendar previous = Calendar.getInstance();
//        previous.setTime(stringToDateWithTime("Tue Apr 11 10:36:33 IST 2017"));
//        Calendar now = Calendar.getInstance();
//        log.info(""+now.getTime());
//        log.info("" + getTime(now.getTime()));
//        log.info("" + getTime(previous.getTime()));
//        log.info("" + 30 * 60 * 1000);
////        long diff = (now.getTime()/60000)
////        log.info(""+diff);
////        if (diff >= 30 * 60 * 1000) {
////            //at least 20 minutes difference
////            log.info("grt");
////        }
//
////        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
////        Date date1 = sdf.parse("2009-12-31");
////        Date date2 = sdf.parse("2010-01-31");
////
////        log.info("date1 : " + sdf.format(date1));
////        log.info("date2 : " + sdf.format(date2));
////
////        Calendar cal1 = Calendar.getInstance();
////        Calendar cal2 = Calendar.getInstance();
////        cal1.setTime(date1);
////        cal2.setTime(date2);
////
////        if (cal1.after(cal2)) {
////            log.info("Date1 is after Date2");
////        }
////
////        if (cal1.before(cal2)) {
////            log.info("Date1 is before Date2");
////        }
////
////        if (cal1.equals(cal2)) {
////            log.info("Date1 is equal Date2");
////        }
////
////        log.info(" DIFF : " + diffTimeStamp("2017-04-10 16:40:37.258", "2017-05-10 16:40:37.258"));
////
////        java.util.Date date = new java.util.Date();
////        log.info("time stamp" + new Timestamp(date.getTime()));
////        log.info(" date min :" + date.getMinutes());
////        Calendar now = Calendar.getInstance();
////        log.info("" + now.get(Calendar.MINUTE));
////        log.info("" + now.get(Calendar.HOUR));
////        if (now.get(Calendar.MINUTE) == date.getMinutes()) {
////            log.info("equals");
////        }
////        SecureRandom random1 = new SecureRandom();
////        log.info(" session id : " + new BigInteger(130, random1).toString(32));
////
////        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
////        StringBuilder sb = new StringBuilder();
////        Random random = new Random();
////        for (int i = 0; i < 20; i++) {
////            char c = chars[random.nextInt(chars.length)];
////            sb.append(c);
////        }
////        String output = sb.toString();
////        log.info(output);
//    }
}
