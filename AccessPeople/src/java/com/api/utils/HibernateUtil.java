package com.api.utils;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    private static final Dolog log = new Dolog(HibernateUtil.class);

    private static SessionFactory factory;
    private static StandardServiceRegistry serviceRegistry = configureRegisterFactory();

    public static StandardServiceRegistry configureRegisterFactory() {
        try {
            Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
            if (serviceRegistry == null) {
                serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                        configuration.getProperties()).build();
            }
            factory = configuration.buildSessionFactory(serviceRegistry);

        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
            throw new ExceptionInInitializerError(ex);
        }

        return serviceRegistry;
    }

    public static SessionFactory getSessionFactory() {
        return factory;
    }

    public static void closeSessionFactory() {
        log.info("Session factory closed");
        factory.close();
    }

    public static void close() throws Exception {
        if (serviceRegistry != null) {
            closeSessionFactory();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }

}
