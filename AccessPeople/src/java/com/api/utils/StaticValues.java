/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.utils;

/**
 *
 * @author suresh
 */
public class StaticValues {

    //Response Codes
    public final static String SUCCESS = "001";
    public final static String FAILURE = "002";
    public final static String SERVER_ERROR = "003";
    public final static String AUTH_KEY_FAILURE = "004";
    public final static String IP_AUTH_FAILURE = "005";
    public final static String QUS_NOT_FOUND = "006";
    public final static String SUB_NOT_FOUND = "007";
    public final static String USER_NOT_FOUND = "008";
    public final static String TIME_EXPIRED = "009";
    public final static String IMPORTANT_FIELD_MISSING = "010";
    public final static String RESULTS_NOT_FOUND = "011";
    public final static String MODULE_CODE_NOT_FOUND = "012";

    // Request //TxnCode
    public final static String LOGIN_REQ = "101";
    public final static String QUS_RES = "102";
    public final static String SYNCGROUPS = "103";

}
