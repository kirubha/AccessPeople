/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.utils;
import org.apache.log4j.Logger;

public class Dolog {

    Logger logger;

    public Dolog(Class c) {
        logger = Logger.getLogger(c);
    }

    public void debug(String msg) {
        logger.debug(msg);
    }

    public void info(String msg) {
        logger.info(msg);
    }

    public void warn(String msg) {
        logger.warn(msg);
    }

     public void error(String msg) {
        logger.error(msg);
    }
}
