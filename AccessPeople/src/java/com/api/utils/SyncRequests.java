/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.utils;

import com.api.entities.RequestResponse;
import com.google.gson.Gson;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author suresh
 */
public class SyncRequests {

    private Gson gson;
    private String responseMsg;
    private RequestResponse requestResponse;

    public SyncRequests(RequestResponse requestResponse) {
        this.gson = new Gson();
        this.requestResponse = requestResponse;
    }

    public String buildResponse() {
        try {
            if (responseMsg.equals(StaticValues.FAILURE)) {
                requestResponse.setResCode(responseMsg);
                requestResponse.setTxnMsg(null);
                requestResponse.setResMsg("Error in processing..");

            } else {
                requestResponse.setTxnMsg(responseMsg);
                requestResponse.setResCode(StaticValues.SUCCESS);
                requestResponse.setResMsg("Successfully completed");
            }
            return gson.toJson(requestResponse);
        } catch (Exception ex) {
            Logger.getLogger(SyncRequests.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
