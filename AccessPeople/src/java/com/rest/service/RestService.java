/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rest.service;

import com.api.controller.AuthenticationController;
import com.api.controller.QuestionController;
import com.api.controller.ResultController;
import com.api.entities.AnswerBean;
import com.api.entities.ClientsBean;
import com.api.entities.QuestionBean;
import com.api.entities.RequestResponse;
import com.api.entities.ResultBean;
import com.api.utils.Dolog;
import com.api.utils.StaticValues;
import com.google.gson.Gson;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author suresh
 */
@Path("/api")
public class RestService {

    private static final Dolog log = new Dolog(RestService.class);
    RequestResponse requestResponse;
    ClientsBean clientsBean = new ClientsBean();
    QuestionBean questionBean = new QuestionBean();
    AnswerBean answerBean = new AnswerBean();
    ResultBean resultBean = new ResultBean();
    AuthenticationController controller = new AuthenticationController();
    QuestionController questionController = new QuestionController();
    ResultController resultController = new ResultController();
    JSONObject jsonobject = new JSONObject();

    public RestService() {
        requestResponse = new RequestResponse();
        gson = new Gson();
    }

    Gson gson = new Gson();

    //Authentication requests
    @POST
    @Path("/login")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response authentication(String data) {
        System.out.println("Entered Data : " + data);
        log.info("Entered Data : " + data);
        try {
            clientsBean = gson.fromJson(data, ClientsBean.class);
            return Response.ok(controller.validateClients(clientsBean)).header("Access-Control-Allow-Origin", "*").build();
        } catch (Exception e) {
            try {
                jsonobject.put("resCode", StaticValues.SERVER_ERROR);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("003"));
                log.info("Response Code : " + jsonobject.toString());
                System.out.println("Exp : " + e.toString());
            } catch (JSONException ex) {
                Logger.getLogger(RestService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return Response.ok(jsonobject.toString()).header("Access-Control-Allow-Origin", "*").build();
        }
    }

    //Question requests
    @POST
    @Path("/question/get/{auth_key}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response questionReq(@PathParam("auth_key") String auth_key, String data) {
        try {
            if (auth_key != null) {
                questionBean = gson.fromJson(data, QuestionBean.class);
                return Response.ok(questionController.getQuestions(questionBean, auth_key)).header("Access-Control-Allow-Origin", "*").build();
            } else {
                log.error("Auth Key Missed");
                jsonobject.put("resCode", StaticValues.IMPORTANT_FIELD_MISSING);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("010"));
                return Response.ok(jsonobject.toString()).header("Access-Control-Allow-Origin", "*").build();
            }

        } catch (Exception e) {
            try {
                log.error("Exp : " + e.toString());
                jsonobject.put("resCode", StaticValues.SERVER_ERROR);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("003"));
            } catch (JSONException ex) {
                Logger.getLogger(RestService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return Response.ok(jsonobject.toString()).header("Access-Control-Allow-Origin", "*").build();
        }
    }

    //Question response
    @POST
    @Path("/answer/update/{auth_key}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response questionRes(@PathParam("auth_key") String auth_key, String data) {
        try {

            if (auth_key != null) {
                log.info("Data : " + data);
                answerBean = gson.fromJson(data, AnswerBean.class);
                return Response.ok(questionController.ansUpdate(answerBean, auth_key)).header("Access-Control-Allow-Origin", "*").build();
            } else {
                log.error("Auth Key Missed");
                jsonobject.put("resCode", StaticValues.IMPORTANT_FIELD_MISSING);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("010"));
                return Response.ok(jsonobject.toString()).header("Access-Control-Allow-Origin", "*").build();
            }
        } catch (Exception e) {
            try {
                log.info(" answerReq Exp : " + e.toString());
                jsonobject.put("resCode", StaticValues.SERVER_ERROR);
                jsonobject.put("resMsg", ResourceBundle.getBundle("config").getString("003"));
            } catch (JSONException ex) {
                Logger.getLogger(RestService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return Response.ok(jsonobject.toString()).header("Access-Control-Allow-Origin", "*").build();
        }
    }

    //Get Results By User
    @POST
    @Path("/result/get/user/{auth_key}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getResultByUser(@PathParam("auth_key") String auth_key) {
        return Response.ok(resultController.getUserBasedResult(auth_key)).header("Access-Control-Allow-Origin", "*").build();
    }

    //Get Results By Clients
    @POST
    @Path("/result/get/client/{client_id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getResultByClients(@PathParam("client_id") String client_id) {
        return Response.ok(resultController.getClientBasedResult(client_id)).header("Access-Control-Allow-Origin", "*").build();
    }

    //Get Results By IP
    @POST
    @Path("/result/get/ip")
//    @Consumes("application/x-www-form-urlencoded")
    public Response getResultByIP(@Context HttpServletRequest req) {
        log.info("HOST : " + req.getRemoteHost());
        return Response.ok(resultController.getIPBasedResult(req.getRemoteHost())).header("Access-Control-Allow-Origin", "*").build();
    }    

}
